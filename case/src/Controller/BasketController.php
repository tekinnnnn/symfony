<?php

namespace App\Controller;

use App\Entity\Basket;
use App\Entity\Product;
use App\Form\BasketType;
use App\Repository\BasketRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/basket")
 */
class BasketController extends AbstractController
{
    /**
     * @Route("/", name="basket_index", methods={"GET"})
     */
    public function index(BasketRepository $basketRepository): Response
    {
        return $this->render(
            'basket/index.html.twig',
            [
                'baskets' => $basketRepository->findAll(),
            ]
        );
    }

    /**
     * @Route("/add/{productId}", name="basket_new", methods={"POST"})
     */
    public function new(Request $request, $productId): Response
    {
        $basketRepository = $this->getDoctrine()->getManager()->getRepository(Basket::class);
        $basket = $basketRepository->findOneBy(['productId' => $productId]);


        $quantity = $request->get('quantity');

        if (!empty($basket) && null != $basket->getId()) {
            $basket->setQuantity($basket->getQuantity() + $quantity);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($basket);
            $entityManager->flush();

            return $this->json(['basketId' => $basket->getId()]);
        }

        $productRepository = $this->getDoctrine()->getManager()->getRepository(Product::class);
        $product = $productRepository->findOneBy(['id' => $productId]);

        $basket = new Basket();
        $basket->setQuantity($quantity)
               ->setProductId($productRepository->find($productId))
               ->setUserId($this->getUser())
               ->setPrice($product->getPrice())
               ->setName($product->getName())
               ->setAddedDateTime(new \DateTime());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($basket);
        $entityManager->flush();

        return $this->json(['basketId' => $basket->getId()]);
    }

    /**
     * @Route("/{id}", name="basket_show", methods={"GET"})
     * @param \App\Entity\Basket $basket
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(Basket $basket): Response
    {
        return $this->render(
            'basket/show.html.twig',
            [
                'basket' => $basket,
            ]
        );
    }

    /**
     * @Route("/{id}/update", name="basket_update", methods={"POST"})
     */
    public function update(Request $request, Basket $basket): Response
    {
        $quantity = $request->get('quantity');
        $basket->setQuantity($quantity);

        $entityManager = $this->getDoctrine()->getManager();
        if (0 == $quantity) {
            $entityManager->remove($basket);
        }
        $entityManager->flush();

        return $this->json(
            [
                'status' => 'success',
                'amount' => $basket->getAmount(),
            ]
        );
    }

    /**
     * @Route("/{id}/edit", name="basket_edit", methods={"GET"})
     */
    public function edit(Request $request, Basket $basket): Response
    {
        $form = $this->createForm(BasketType::class, $basket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('basket_index');
        }

        return $this->render(
            'basket/edit.html.twig',
            [
                'basket' => $basket,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}", name="basket_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Basket $basket): Response
    {
        if ($this->isCsrfTokenValid('delete' . $basket->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($basket);
            $entityManager->flush();
        }

        return $this->redirectToRoute('basket_index');
    }
}
