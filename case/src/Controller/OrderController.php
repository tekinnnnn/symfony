<?php

namespace App\Controller;

use App\Entity\Basket;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Form\OrderType;
use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/order")
 */
class OrderController extends AbstractController
{
    /**
     * @Route("/", name="order_index", methods={"GET"})
     */
    public function index(OrderRepository $orderRepository): Response
    {
        $criteria = [
            'approveDate' => null,
        ];

        if (!$this->isGranted('ROLE_ADMIN')) {
            $criteria['userId'] = $this->getUser()->getId();
        }

        return $this->render('order/index.html.twig', [
            'orders' => $orderRepository->findBy($criteria),
        ]);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("complete", name="order_complete")
     */
    public function new(Request $request): JsonResponse
    {
        $order = new Order();
        $order->setUserId($this->getUser())
              ->setOrderDate(new \DateTime())
              ->setAddress($request->get('address'));

        $entityManager = $this->getDoctrine()->getManager();
        $basketCollection = $entityManager->getRepository(Basket::class)->findAll();

        foreach ($basketCollection as $basket) {
            $orderItem = new OrderItem();
            $orderItem->setProductId($basket->getProductId())
                      ->setQuantity($basket->getQuantity());
            $order->addOrderItem($orderItem);

            $entityManager->persist($orderItem);
        }

        $entityManager->persist($order);
        $entityManager->flush();

        if (count($order->getOrderItems()) === count($basketCollection)) {
            array_map(
                function ($basket) use ($entityManager) {
                    $entityManager->remove($basket);
                    $entityManager->flush();
                },
                $basketCollection
            );
        }

        return $this->json(['orderId' => $order->getId()]);
    }

    /**
     * @Route("/{id}", name="order_show", methods={"GET"})
     */
    public function show(Order $order): Response
    {
        return $this->render('order/show.html.twig', [
            'order' => $order,
        ]);
    }

    /**
     * @Route("/{id}/approve", name="order_approve", methods={"GET","POST"})
     */
    public function approve(Request $request, Order $order): Response
    {
        $order->setApproveDate(new \DateTime());

        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('order_index');
    }

    /**
     * @Route("/{id}/cancel", name="order_cancel", methods={"GET","DELETE"})
     */
    public function delete(Request $request, Order $order): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($order);
        $entityManager->flush();

        return $this->redirectToRoute('order_index');
    }
}
